﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PCLUtil_cs
{
    public static class PCUtil
    {
        // Beacause handling short array you only need to use B and G channel
        public static byte[] DepthDataToByteArray(short[] depthData)
        {
            var byteArray = new byte[depthData.Length * 4];
            var byteArrayIdx = 0;
            short depth;
            for (int i = 0; i < depthData.Length; i++)
            {
                depth = depthData[i];
                if (depth < 0) depth = 0;
                byteArray[byteArrayIdx++] = (byte)(depth & 0x00ff); //b 
                byteArray[byteArrayIdx++] = (byte)((depth & 0xff00) >> 8); //g 
                byteArray[byteArrayIdx++] = 0; //r
                byteArray[byteArrayIdx++] = 255; //a for visibility
            }
            return byteArray;
        }
        public static WriteableBitmap ConvertDepthValToDepthImage(short[] depthData, int imgWidth, int imgHeight, int dpi = 96, PixelFormat? pixelFormat = null)
        {
            if (pixelFormat == null) pixelFormat = PixelFormats.Bgr32;
            var wb = new WriteableBitmap(imgWidth, imgHeight, dpi, dpi, pixelFormat.Value, null);
            wb.WritePixels(new Int32Rect(0, 0, imgWidth, imgHeight), DepthDataToByteArray(depthData), imgWidth * sizeof(int), 0);
            return wb;
        }

        static T[] convertDepthImageToDepthVal<T>(WriteableBitmap depthBitmap, Func<int, T> castFunc)
        {
            T[] depthData = null;
            int w = depthBitmap.PixelWidth; int h = depthBitmap.PixelHeight;
            var argbPixels = new int[w * h];
            depthData = new T[w * h];
            depthBitmap.CopyPixels(argbPixels, 4 * w, 0);
            Parallel.For(0, h, row =>
            {
                int idx = row * w;
                for (int col = 0; col < w; col++)
                {
                    depthData[idx] = castFunc(argbPixels[idx]);
                    idx++;
                }
            });
            return depthData;
        }
        /// <summary>
        /// Method for loading depth image and convert it to depth data
        /// </summary>
        /// <param name="depthImageDirPath"></param>
        /// <returns></returns>
        public static short[] ConvertDepthImageToShortDepthVal(WriteableBitmap depthBitmap)
        {
            return convertDepthImageToDepthVal<short>(depthBitmap, (pixel) => ((short)pixel));
        }
        public static ushort[] ConvertDepthImageToUShortDepthVal(WriteableBitmap depthBitmap)
        {
            return convertDepthImageToDepthVal<ushort>(depthBitmap, (pixel) => ((ushort)(pixel & 0x0000FFFF)));
        }

        // Export depth data as Point Cloud Data for PCL
        public static void ConvertDepthImageToPcd_Px(string pcdPath, WriteableBitmap depthBitmap)
        {
            var w = depthBitmap.PixelWidth;
            var h = depthBitmap.PixelHeight;
            var depthData = ConvertDepthImageToShortDepthVal(depthBitmap);

            using (var sw = new StreamWriter(pcdPath))
            {
                // using FFF because pcl's visualizer is not actully compatible with III format
                sw.WriteLine(new PCDHeader("x y z".Split(' '), "F F F".Split(' '), w, h).ToString());
                long pixelIdx = 0;
                for (int y = 0; y < h; y++)
                {
                    for (int x = 0; x < w; x++)
                    {
                        sw.WriteLine(string.Join(" ", new[] { x, y, depthData[pixelIdx] }));
                        pixelIdx++;
                    }
                }
            }
        }

        // Export depth data as Point Cloud Data for PCL
        public static void ConvertDepthImageToPcd(string pcdPath, WriteableBitmap depthBitmap, short depthMin = 0, short depthMax = short.MaxValue)
        {
            var w = depthBitmap.PixelWidth;
            var h = depthBitmap.PixelHeight;
            var rawDepthData = ConvertDepthImageToShortDepthVal(depthBitmap);

            var dataList = new List<double>[3];
            for (int i = 0; i < 3; i++) { dataList[i] = new List<double>(w*h); }

            // for transforming (0,0) to center of pointcloud
            var centerX = w/2;
            var centerY = h/2;
            long pixelIdx = 0;
            int actualPixelNum = 0;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    var depth = rawDepthData[pixelIdx];
                    if(depthMin < depth && depth < depthMax){
                        var realX = PCUtil.ConvertPixelToRealWorldLength(x - centerX, depth);
                        var realY = PCUtil.ConvertPixelToRealWorldLength(y - centerY, depth);
                        if (double.IsNaN(realX) == false && double.IsNaN(realY) == false)
                        {
                            dataList[0].Add(realX);
                            dataList[1].Add(realY);
                            dataList[2].Add(depth / 1000.0);
                            actualPixelNum++;
                        }
                    }
                    pixelIdx++;
                }
            }
            var fields = "x y z".Split(' ');
            var data = Enumerable.Range(0, 3).ToDictionary(i => fields[i], i => (dynamic)dataList[i]);
            var pc = PointCloud.Create(new PCDHeader(fields, "F F F".Split(' '), actualPixelNum, 1), data);
            pc.Save(pcdPath);
        }


        // Convert real world length to pixel length using Kinect's calibrated parameter.
        public static int ConvertRealWorldToPixelLength(float realWorldLength_m, short depth_mm)
        {
            // Calculate ? px/m at given depth
            return (int)Math.Round(realWorldLength_m * 1151500 / Math.Pow(depth_mm, 1.012));
        }

        // Convert pixel length to real world length using Kinect's calibrated parameter.
        public static float ConvertPixelToRealWorldLength(int pixelLength, short depth_mm)
        {
            // Calculate ? m/px at given depth
            return (float)(pixelLength * Math.Pow(depth_mm, 1.012) / 1151500);
        }

        static T executePCLCommand<T>(string argmuments, Func<string, T> resultCastFunc)
        {
            var p = new Process(); // Create process object
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true; // enable to read standard output
            p.StartInfo.RedirectStandardInput = false;
            p.StartInfo.CreateNoWindow = true; // do not show window whill processing
            p.StartInfo.Arguments = argmuments;
            p.StartInfo.FileName = @"PCLUtilExe.exe";
            p.Start();

            string results = p.StandardOutput.ReadToEnd(); // read standard output
            p.WaitForExit(); // wait until finish reading process's output
            p.Close();
            return resultCastFunc(results);
        }

        public static string ExecutePCLCommand_Str(string argmuments)
        {
            return executePCLCommand(argmuments, (string res) => res);
        }

        public static double ExecutePCLCommand_Dbl(string argmuments)
        {
            return executePCLCommand(argmuments, (string res) => double.Parse(res));
        }

        /// <summary>
        /// for passing path argment to process
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string DoubleQuote(this string input)
        {
            return new StringBuilder().Append("\"").Append(input).Append("\"").ToString();
        }

    }
}
