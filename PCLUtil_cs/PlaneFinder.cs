﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;

namespace PCLUtil_cs
{
    /// <summary>
    /// Class for finding plane from 3d data using RANSAC algorithm
    /// </summary>
    public static class PlaneFinder
    {
        // Define distance range from plane which for calculation in mm
        public const int PLANE_DEPTH_MIN = 500;
        public const int PLANE_DEPTH_MAX = 1500;
        // Define max angle from kinect view
        public const double PLANE_ANGLE_MAX = 30;

        // Class of Plane
        public class Plane
        {
            public double a, b, c, d;
            double _a, _b, _d;
            private Rect planeSpace;
            public Rect PlaneSpace { get { return planeSpace; } }

            // Any surface can be describe in ax + by + cz + d = 0 
            public Plane(double a, double b, double c, double d)
            {
                this.a = a;
                this.b = b;
                this.c = c;
                this.d = d;
                devideCoefficient();
            }

            public Plane(Point3D p1, Point3D p2, Point3D p3)
            {
                this.a = (p2.Y - p1.Y) * (p3.Z - p1.Z) - (p3.Y - p1.Y) * (p2.Z - p1.Z);
                this.b = (p2.Z - p1.Z) * (p3.X - p1.X) - (p3.Z - p1.Z) * (p2.X - p1.X);
                this.c = (p2.X - p1.X) * (p3.Y - p1.Y) - (p3.X - p1.X) * (p2.Y - p1.Y);
                this.d = -(a * p1.X + b * p1.Y + c * p1.Z);
                devideCoefficient();
            }

            void devideCoefficient()
            {
                if (this.c == 0)
                {
                    _a = _b = _d = 0;
                }
                else
                {
                    _a = -a / c;
                    _b = -b / c;
                    _d = -d / c;
                }
            }

            public bool IsXYBelowAngle(double angleDegree)
            {
                return Math.Abs(_a) < angleDegree && Math.Abs(_b) < angleDegree;
            }

            public double CalcZValue(double x, double y)
            {
                // if c == 0 then it means plane is on x-y axis
                return _a * x + _b * y + _d;
            }
            public double CalcZValue(Point p)
            {
                return CalcZValue(p.X, p.Y);
            }
            public double CalcAbsZDiff(Point3D p)
            {
                return Math.Abs(CalcZValue(p.X, p.Y) - p.Z);
            }

            // calculate plane space and set to planeSpace
            public void CalcPlaneSpace(List<Point3D> samplePoints)
            {
                // remove noise points by removing surrounding points
                var rowCountDict = new Dictionary<int, int>();
                var colCountDict = new Dictionary<int, int>();
                foreach (var points in samplePoints)
                {
                    var intRow = (int)points.Y; var intCol = (int)points.X;
                    if (rowCountDict.ContainsKey(intRow) == false)
                    {
                        rowCountDict.Add(intRow, 1);
                    }
                    else
                    {
                        rowCountDict[intRow]++;
                    }
                    if (colCountDict.ContainsKey(intCol) == false)
                    {
                        colCountDict.Add(intCol, 1);
                    }
                    else
                    {
                        colCountDict[intCol]++;
                    }
                }
                var rowMaxPointNum = rowCountDict.Max(kv => kv.Value);
                var colMaxPointNum = colCountDict.Max(kv => kv.Value);
                // 最大の横幅よりも60%以内に
                var deskRowList = rowCountDict.Where(kv => kv.Value > rowMaxPointNum * 0.6).Select(kv => kv.Key).OrderBy(row => row);
                var deskColList = colCountDict.Where(kv => kv.Value > colMaxPointNum * 0.6).Select(kv => kv.Key).OrderBy(col => col);
                planeSpace = new Rect(new Point(deskColList.First(), deskRowList.First()), new Point(deskColList.Last(), deskRowList.Last()));
            }
        }

        // Class for calculating average of multiple planes in time sequences
        public class AveragePlane
        {
            public delegate void AverageFinishedDelegate(object sender);
            public event AverageFinishedDelegate AverageFinished;

            private const int PLANE_DETECT_LIMIT = 20;
            private const int RELIABLE_DETECTION_NUM = 5;
            private int planeFindCount = 0;
            private double ave_a, ave_b, ave_c, ave_d;
            private ConcurrentDictionary<Point3D, int> detectedPointCount = new ConcurrentDictionary<Point3D, int>();
            Task[] taskArr = new Task[PLANE_DETECT_LIMIT];

            public bool IsPlaneDetectEnough { get; private set; }
            public bool IsAverageFinished { get; private set; }
            public Plane AveragedPlane { get; private set; }
            public IEnumerable<Point3D> ReliablePoints { get; private set; }

            public AveragePlane()
            {
                IsPlaneDetectEnough = false;
                IsAverageFinished = false;
            }

            public void FindPlane(short[] depthData, int width, int height, short minDepth = PLANE_DEPTH_MIN, short maxDepth = PLANE_DEPTH_MAX)
            {
                if (planeFindCount >= PLANE_DETECT_LIMIT)
                {
                    IsPlaneDetectEnough = true;
                    // if all tasks are finished
                    Task.Factory.ContinueWhenAll(taskArr, t =>
                    {
                        // take frequently detected points as reliable
                        ReliablePoints = detectedPointCount.Where(kv => kv.Value > RELIABLE_DETECTION_NUM).Select(kv => kv.Key);
                        AveragedPlane = new PlaneFinder.Plane(ave_a / PLANE_DETECT_LIMIT, ave_b / PLANE_DETECT_LIMIT, ave_c / PLANE_DETECT_LIMIT, ave_d / PLANE_DETECT_LIMIT);
                        AveragedPlane.CalcPlaneSpace(ReliablePoints.ToList());

                        IsAverageFinished = true; // finished finding last plane for average
                        if (AverageFinished != null) AverageFinished(this);
                    });
                }
                else
                {
                    taskArr[planeFindCount] = Task<Tuple<Plane, List<Point3D>>>.Factory.StartNew(()=>PlaneFinder.PlaneFind(depthData, width, height, minDepth: minDepth, maxDepth: maxDepth))
                    .ContinueWith(t =>
                    {
                        var detectedPlane = t.Result.Item1;
                        ave_a += detectedPlane.a; ave_b += detectedPlane.b;
                        ave_c += detectedPlane.c; ave_d += detectedPlane.d;
                        t.Result.Item2.ForEach(p =>
                        {
                            if (detectedPointCount.ContainsKey(p) == false)
                            {
                                detectedPointCount.TryAdd(p, 1);
                            }
                            else
                            {
                                detectedPointCount[p]++;
                            }
                        });
                    });
                    planeFindCount++;
                }
            }

            //public Canvas DrawAveragePlane(Canvas canvas)
            //{
            //    var rectangle = new System.Windows.Shapes.Rectangle() { Height = AveragedPlane.PlaneSpace.Height, Width = AveragedPlane.PlaneSpace.Width, StrokeThickness = 5, Stroke = Brushes.Red };
            //    rectangle.RenderTransform = new TranslateTransform(AveragedPlane.PlaneSpace.Left, AveragedPlane.PlaneSpace.Top);
            //    canvas.Children.Add(rectangle);
            //    return canvas;
            //}
        }

        // Select random points from defined area(ROI)
        static Point3D PickRandomPoint(short[] depthData, int depthWidth, int depthHeight, Random random,
            int xMin = 0, int xMax = -1, int yMin = 0, int yMax = -1, int zMin = PLANE_DEPTH_MIN, int zMax = PLANE_DEPTH_MAX)
        {
            if (xMax < 0) xMax = depthWidth;
            if (yMax < 0) yMax = depthHeight;
            if (xMax < xMin || yMax < yMin || zMax < zMin) throw new ArgumentException("Min value has to be smaller than Max value");
            short depthVal = 0; int xIdx, yIdx;
            do
            {
                xIdx = (int)((xMax - xMin) * random.NextDouble());
                yIdx = (int)((yMax - yMin) * random.NextDouble());
                int depthIdx = yIdx * depthWidth + xIdx;
                depthVal = depthData[depthIdx];
            }
            while (depthVal < zMin || zMax < depthVal);
            return new Point3D(xIdx, yIdx, depthVal);
        }

        // Create random plane using defined angle and depth
        static Plane CreateRandomPlane(short[] depthData, int depthWidth, int depthHeight, Random random,
            int xMin = 0, int xMax = -1, int yMin = 0, int yMax = -1, int zMin = PLANE_DEPTH_MIN, int zMax = PLANE_DEPTH_MAX, double angleLimitDegree = 30)
        {
            Plane samplePlane = null;
            var angleLimitRadian = Math.Atan(angleLimitDegree);
            do
            {
                var p1 = PickRandomPoint(depthData, depthWidth, depthHeight, random, xMin, xMax, yMin, yMax, zMin, zMax);
                var p2 = PickRandomPoint(depthData, depthWidth, depthHeight, random, xMin, xMax, yMin, yMax, zMin, zMax);
                var p3 = PickRandomPoint(depthData, depthWidth, depthHeight, random, xMin, xMax, yMin, yMax, zMin, zMax);
                samplePlane = new Plane(p1, p2, p3);
            }
            while (samplePlane.IsXYBelowAngle(angleLimitDegree) == false);
            return samplePlane;
        }

        public static Tuple<Plane, List<Point3D>> PlaneFind(short[] depthData, int depthWidth, int depthHeight,
            Rect? roi = null, short minDepth = PLANE_DEPTH_MIN, short maxDepth = PLANE_DEPTH_MAX, double angleLimitDegree = PLANE_ANGLE_MAX,
            int samplePlaneNum = 500, short strictPlaneRange = 5, short admitPlaneRange = 8, int pointSearchInterval = 20)
        {
            var strictPlaneCounts = new int[samplePlaneNum]; // number of points that are strictly contained by defined plane
            var admitPlanePoints = new Point3D[samplePlaneNum, samplePlaneNum * 4 * 20 / pointSearchInterval]; // number of points that are less strictly contained by defined plane
            var admitPlaneCounts = new int[samplePlaneNum];

            var xMin = roi == null ? 0 : (int)roi.Value.Left;
            var xMax = roi == null ? depthWidth : (int)roi.Value.Right;
            var yMin = roi == null ? 0 : (int)roi.Value.Top;
            var yMax = roi == null ? depthHeight : (int)roi.Value.Bottom;

            var candidatePlanes = new Plane[samplePlaneNum];
            var random = new Random();
            // Create random planes which angle is in range of angleLimitDegree
            for (int planeIdx = 0; planeIdx < samplePlaneNum; planeIdx++)
            {
                candidatePlanes[planeIdx] = CreateRandomPlane(depthData, depthWidth, depthHeight, random, xMin, xMax, yMin, yMax, minDepth, maxDepth, angleLimitDegree);
            }

            Parallel.For(0, (yMax - yMin) / pointSearchInterval, row =>
            {
                row = pointSearchInterval * row + yMin;
                for (int col = xMin; col < xMax; col += pointSearchInterval)
                {
                    var depthIdx = row * depthWidth + col;
                    var depthVal = depthData[depthIdx];
                    if (depthVal < minDepth || maxDepth < depthVal) continue;
                    var samplePoint = new Point3D(col, row, depthVal);
                    for (int planeIdx = 0; planeIdx < samplePlaneNum; planeIdx++)
                    {
                        var plane = candidatePlanes[planeIdx];
                        var planeZDiff = plane.CalcAbsZDiff(samplePoint);
                        if (planeZDiff <= strictPlaneRange)
                        {
                            strictPlaneCounts[planeIdx]++;

                            var admitPlanePointIdx = admitPlaneCounts[planeIdx];
                            admitPlanePoints[planeIdx, admitPlanePointIdx] = samplePoint;
                            admitPlaneCounts[planeIdx] = admitPlanePointIdx + 1;
                        }
                        else if (planeZDiff <= admitPlaneRange)
                        {
                            var admitPlanePointIdx = admitPlaneCounts[planeIdx];
                            admitPlanePoints[planeIdx, admitPlanePointIdx] = samplePoint;
                            admitPlaneCounts[planeIdx] = admitPlanePointIdx + 1;
                        }
                    }
                }
            });

            var maxPointNumPlaneTpl = strictPlaneCounts.Select((val, idx) => new { val, idx }).OrderByDescending(a => a.val).ToArray()[0];
            var detectedPlane = candidatePlanes[maxPointNumPlaneTpl.idx];
            var planePoints = new List<Point3D>(maxPointNumPlaneTpl.val);
            for (int pointIdx = 0; pointIdx < maxPointNumPlaneTpl.val; pointIdx++)
            {
                planePoints.Add(admitPlanePoints[maxPointNumPlaneTpl.idx, pointIdx]);
            }
            detectedPlane.CalcPlaneSpace(planePoints);
            // return best support plane and list of points from depthdata which are inside of the plane
            return new Tuple<Plane, List<Point3D>>(detectedPlane, planePoints);
        }

        //public static Canvas CreateDottedCanvas(int width, int height, IEnumerable<Point3D> points, Brush color = null, int circleRadius = 5)
        //{
        //    if (color == null) color = Brushes.LightGreen;
        //    var canvas = new Canvas() { Width = width, Height = height };
        //    foreach (var p3d in points)
        //    {
        //        var ellipse = new System.Windows.Shapes.Ellipse() { Stroke = Brushes.Red, StrokeThickness = 5, Height = circleRadius * 2, Width = circleRadius * 2 };
        //        ellipse.RenderTransform = new TranslateTransform(p3d.X, p3d.Y);
        //        canvas.Children.Add(ellipse);
        //    }
        //    return canvas;
        //}
    } 
}
