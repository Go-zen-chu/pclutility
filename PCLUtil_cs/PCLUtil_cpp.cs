﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PCLUtil_cs
{
    public static class PCLUtil_cpp
    {
        // this function is slow compared to rewriting pcd file
        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static void FilterPointCloud_XYZ(string src_path, string dst_path, string field_name, double limit_min, double limit_max);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
		public extern static void VoxelGridDownSample_XYZ(string src_path, string dst_path, double leafSize);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
		public extern static void SmoothPointCloud_XYZ_XYZN(string src_path, string dst_path, double radius);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static void SmoothPointCloud_XYZN_XYZN(string src_path, string dst_path, double radius);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static double CalcSurfaceArea_XYZN(string pcd_path, double radius, double mu);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static double GetAverageCurvature_XYZN(string pcd_path);
        
        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
		public extern static double IterativeClosestPoint_Gen_XYZN(string src_path, string dst_path, double dx, double dy, double dz);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static void RegionGrowingClusteringXYZ(string src_path, int ksearch);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static void Visualize_XYZ(string pcd_path);

        [DllImport("PCLUtil_cpp.dll", CallingConvention = CallingConvention.Cdecl)]
        public extern static void Visualize_XYZN(string pcd_path);
    }
}
