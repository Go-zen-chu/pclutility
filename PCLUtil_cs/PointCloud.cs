﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PCLUtil_cs
{
    public class PCDHeader
    {
        string version = ".7";
        public string Version { get { return version; }  }

        string[] fields;
        /// <summary>
        /// Defines the name of data fields (e.g. x y z rgb normal_x curvature)
        /// </summary>
        public string[] Fields { get { return fields; } }

        int[] size;
        /// <summary>
        /// Defines each fields value size in byte
        /// </summary>
        public int[] Size { get { return size; } }

        string[] type;
        /// <summary>
        /// Defines what kind of value type the data have ( I: integer, F: double) 
        /// </summary>
        public string[] Type { get { return type; } }

        int[] count;
        public int[] Count { get { return count; } }

        int width;
        public int Width { get { return width; } set { this.width = value; } }

        int height;
        public int Height { get { return height; } }

        public int Points { get { return width * height; } }

        int[] viewpoint;
        public int[] ViewPoint { get { return viewpoint; } }

        string dataType = "ascii";
        public string DataType { get { return dataType; } }

        public PCDHeader(string version, string[] fields, int[] size, string[] type, int[] count, int width, int height, int[] viewpoint, string dataType)
        {
            initialize(version, fields, size, type, count, width, height, viewpoint, dataType);
        }

        public PCDHeader(string[] fields, string[] type, int width, int height)
        {
            var fieldsNum = fields.Length;
            var size = Enumerable.Range(0, fieldsNum).Select(idx => 4); //TODO: fix size with 4 that should be able to change
            var count = Enumerable.Range(0, fieldsNum).Select(idx => 1); // fix count with 1 which is default
            var viewpoint = new[]{ 0, 0, 0, 1, 0, 0, 0};
            initialize(".7", fields, size.ToArray(), type, count.ToArray(), width, height, viewpoint, "ascii");
        }

        public PCDHeader(IEnumerable<string> headerLines)
        {
            foreach (var headerLine in headerLines)
            {
                var valueArr = headerLine.Split(new[] { ' ' }, 2); // split into FORMAT and FORMAT_DATA
                #region attr
                switch (valueArr[0].ToLower())
                {
                    case "version":
                        {
                            this.version = valueArr[1];
                        } break;
                    case "fields":
                        {
                            var lowerField = valueArr[1].ToLower();
                            this.fields = lowerField.Split(' ');
                        } break;
                    case "size":
                        {
                            this.size = valueArr[1].Split(' ').Select(strVal => int.Parse(strVal)).ToArray();
                        } break;
                    case "type":
                        {
                            var upperType = valueArr[1].ToUpper();
                            this.type = upperType.Split(' ');
                        } break;
                    case "count":
                        {
                            this.count = valueArr[1].Split(' ').Select(strVal => int.Parse(strVal)).ToArray();
                        } break;
                    case "width":
                        {
                            this.width = int.Parse(valueArr[1]);
                        } break;
                    case "height":
                        {
                            this.height = int.Parse(valueArr[1]);
                        } break;
                    case "viewpoint":
                        {
                            this.viewpoint = valueArr[1].Split(' ').Select(strVal => int.Parse(strVal)).ToArray();
                        } break;
                    case "points":
                        {
                            // unneed because points equals to height and width
                        } break;
                    case "data":
                        {
                            this.dataType = valueArr[1];
                        } break;
                    default:
                        throw new Exception("Unexpected Format");
                }
                #endregion
            }
        }

        private void initialize(string version, string[] fields, int[] size, string[] type, int[] count, int width, int height, int[] viewpoint, string dataType)
        {
            if (fields == null || size == null || type == null || count == null || viewpoint == null) throw new ArgumentNullException();
            var totalCount = count.Sum(); // total count defines the number of elements
            if (fields.Length != totalCount || size.Length != totalCount || type.Length != totalCount) throw new ArgumentException("The length of array fields, size, type should be " + totalCount);
            if (viewpoint.Length != 7) throw new ArgumentException("The length of viewpoint should be 7");

            this.version = version;
            this.fields = fields;
            this.size = size;
            this.type = type;
            this.count = count;
            this.width = width;
            this.height = height;
            this.viewpoint = viewpoint;
            this.dataType = dataType;
        }

        public override string ToString()
        {
            var headerStrArr = new[] {
                string.Format("VERSION {0}", version),
                string.Format("FIELDS {0}", string.Join(" ", fields)),  
                string.Format("SIZE {0}", string.Join(" ", size)), 
                string.Format("TYPE {0}", string.Join(" ", type)), 
                string.Format("COUNT {0}", string.Join(" ", count)),
                string.Format("WIDTH {0}", width),
                string.Format("HEIGHT {0}", height),
                string.Format("VIEWPOINT {0}", string.Join(" ", viewpoint)),
                string.Format("POINTS {0}", width * height),
                string.Format("DATA {0}", dataType)
            };
            return string.Join(Environment.NewLine, headerStrArr);
        }
    }

    /// <summary>
    /// Point Cloud data that have point data in dynamic dictionary
    /// </summary>
    public class PointCloud
    {
        PCDHeader header;
        public PCDHeader Header { get { return header; } }

        Dictionary<string, dynamic> data;
        /// <summary>
        /// key is field, value is an list of key field
        /// </summary>
        public Dictionary<string, dynamic> Data { get { return data; } }

        // Closure 
        private PointCloud() {}

        public static PointCloud Create(PCDHeader header, Dictionary<string, dynamic> data)
        {
            var pc = new PointCloud();
            pc.header = header;
            pc.data = data;
            return pc;
        }

        public static PointCloud Load(string pcdPath)
        {
            if (File.Exists(pcdPath) == false) throw new FileNotFoundException();
            var pc = new PointCloud();
            
            bool dataFlag = false;
            int dataRowIdx = 0;
            List<string> headerLines = new List<string>(); 
            using (var sr = new StreamReader(pcdPath))
            {
                while (sr.Peek() >= 0)
                {
                    var pcdLine = sr.ReadLine();
                    if (pcdLine.StartsWith("#")) continue; // ignore comment
                    if(dataFlag == false){
                        headerLines.Add(pcdLine);
                        if (pcdLine.ToUpper().StartsWith("DATA"))
                        {
                            dataFlag = true;
                            pc.header = new PCDHeader(headerLines); // end parsing header
                        }
                    }else{
                        var valueArr = pcdLine.Split(' ');

                        // initialize data array
                        if (dataRowIdx == 0)
                        {
                            // second element has to be dynamic since it has different array type
                            var field_type = pc.header.Fields.Zip(pc.header.Type, (fld, ty) => new { fld, ty });
                            pc.data = field_type.Select(a =>
                            {
                                switch (a.ty)
                                {
                                    case "U": return new Tuple<string, dynamic>(a.fld, new List<uint>(pc.header.Points));
                                    case "I": return new Tuple<string, dynamic>(a.fld, new List<int>(pc.header.Points));
                                    case "F": return new Tuple<string, dynamic>(a.fld, new List<double>(pc.header.Points));
                                }
                                throw new Exception("Unexpected Numeric Type");
                            }).ToDictionary(tpl => tpl.Item1, tpl => tpl.Item2);
                        }
                        if (valueArr.Any(valStr => valStr.ToLower() == "nan")) // bad format data contained
                        {
                            if (pc.Header.Height == 1)
                            {
                                pc.header.Width -= 1; // reduce number of points
                                Debug.WriteLine("Nan find in Line:{0} in data", dataRowIdx);
                            }
                            else
                                throw new Exception("Find NaN in dataset");
                        }
                        else
                        {
                            for (int colIdx = 0; colIdx < valueArr.Length; colIdx++)
                            {
                                var fld = pc.header.Fields[colIdx];
                                var type = pc.header.Type[colIdx];
                                switch (type)
                                {
                                    case "U": pc.data[fld].Add(uint.Parse(valueArr[colIdx])); break;
                                    case "I": pc.data[fld].Add(int.Parse(valueArr[colIdx])); break;
                                    case "F": pc.data[fld].Add(double.Parse(valueArr[colIdx])); break;
                                }
                            }
                            dataRowIdx++;
                        }
                    }
                }
            }
            return pc;
        }

        public void Save(string pcdPath)
        {
            using (var sw = new StreamWriter(pcdPath))
            {
                // write header
                sw.WriteLine(header.ToString());
                // write data
                var dataArr = header.Fields.Select(field => data[field]).ToArray();
                var colNum = header.Fields.Length;
                for (int row = 0; row < header.Points; row++) // write each row
                {
                    sw.Write(dataArr[0][row]);
                    for (int col = 1; col < colNum; col++)
                    {
                        sw.Write(" " + dataArr[col][row]);
                    }
                    sw.WriteLine();
                }
            }
        }

        /// <summary>
        /// Remove nan data from pcd file
        /// </summary>
        /// <param name="pcdPath"></param>
        /// <returns></returns>
        public static bool RemoveNanInPCD(string pcdPath)
        {
            if (File.Exists(pcdPath) == false) throw new FileNotFoundException();

            var pcdLines = new List<string>();
            bool dataFlag = false;
            int dataWidth = -1;
            int rowIdx = 0;
            int widthRowIdx = -1;
            int pointsRowIdx = -1;
            var needsToBeRepaired = false;

            using (var sr = new StreamReader(pcdPath))
            {
                while (sr.Peek() >= 0)
                {
                    var pcdLine = sr.ReadLine();
                    if (dataFlag == false)
                    {
                        var valueArr = pcdLine.Split(new[] { ' ' }, 2); // split into FORMAT and FORMAT_DATA
                        #region attr
                        switch (valueArr[0].ToLower())
                        {
                            case "width":
                                {
                                    dataWidth = int.Parse(valueArr[1]);
                                    widthRowIdx = rowIdx;
                                } break;
                            case "height":
                                {
                                    int dataHeight = int.Parse(valueArr[1]);
                                    if (dataHeight != 1) throw new NotImplementedException("Compatible with unordered pcd");
                                } break;
                            case "points":
                                {
                                    pointsRowIdx = rowIdx;
                                } break;
                            case "data":
                                {
                                    dataFlag = true;
                                } break;
                        }
                        #endregion
                        pcdLines.Add(pcdLine);
                    }
                    else
                    {
                        if (pcdLine.Contains("nan"))
                        {
                            dataWidth -= 1;
                            needsToBeRepaired = true;
                        }
                        else
                        {
                            pcdLines.Add(pcdLine);
                        }
                    }
                    rowIdx++;
                }
            }
            if (needsToBeRepaired)
            {
                using (var sw = new StreamWriter(pcdPath))
                {
                    for (int i = 0; i < pcdLines.Count; i++)
                    {
                        if (i == widthRowIdx) sw.WriteLine("WIDTH " + dataWidth);
                        else if (i == pointsRowIdx) sw.WriteLine("POINTS " + dataWidth);
                        else
                            sw.WriteLine(pcdLines[i]);
                    }
                }
            }
            return needsToBeRepaired;
        }
    }

    /// <summary>
    /// Point data with integer (You can use Point3D for double data)
    /// </summary>
    public struct PointInt3D
    {
        int x;
        public int X { get { return x; } set { this.x = value; } }
        int y;
        public int Y { get { return y; } set { this.y = value; } }
        int z;
        public int Z { get { return z; } set { this.z = value; } }

        public static PointInt3D operator -(PointInt3D pt)
        {
            return new PointInt3D(-pt.X, -pt.Y, -pt.Z);
        }
        public static PointInt3D operator -(PointInt3D p1, PointInt3D p2)
        {
            return new PointInt3D(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
        }
        public static PointInt3D operator +(PointInt3D p1, PointInt3D p2)
        {
            return new PointInt3D(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z);
        }

        public PointInt3D(int x, int y, int z)
        {
            this.x = x; this.y = y; this.z = z;
        }
    }
}
