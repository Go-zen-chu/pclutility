﻿#include "stdafx.h"
#include "../PCLUtil_cpp/stdafx.h"
#include "../PCLUtil_cpp/PCLUtil_cpp.h"

using namespace PCLUtil_cpp;
using std::string;


void visualizeTest(string src_pcd_path){
	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path, *src_cloud);
	// test target
	PCLUtil_cpp::Visualize(src_cloud);
}

void removeOutlierTest(string src_pcd_path, string dst_pcd_path){
	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path, *src_cloud);
	PCxyz::Ptr dst_cloud(new PCxyz);
	// test target
	PCLUtil_cpp::RemoveOutlierStatistic(src_cloud, dst_cloud);

	pcl::io::savePCDFileASCII(dst_pcd_path, *dst_cloud);
}

void voxelGridDownSampleTest(string src_pcd_path, string dst_pcd_path){
	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path, *src_cloud);
	PCxyz::Ptr dst_cloud(new PCxyz);
	// test target
	PCLUtil_cpp::VoxelGridDownSample(src_cloud, dst_cloud, 0.02);

	pcl::io::savePCDFileASCII(dst_pcd_path, *dst_cloud);
}

void calcNormalTest(){
	std::string src_pcd_path_str = R"(C:\Users\headl\Desktop\pclutility\PCLUtil_cppTest\data\bunny.pcd)";

	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path_str, *src_cloud);

	PCTPtr<pcl::PointNormal> dst_cloud(new PCxyzn);
	PCLUtil_cpp::CalcNormal(src_cloud, dst_cloud);

	pcl::io::savePCDFileASCII(R"(C:\Users\headl\Desktop\pclutility\PCLUtil_cppTest\data\bunny_n.pcd)", *dst_cloud);
}

void getAverageCurvatureTest(string src_pcd_path){
	pcl::PointCloud<pcl::PointNormal>::Ptr src_cloud(new pcl::PointCloud<pcl::PointNormal>);
	pcl::io::loadPCDFile(src_pcd_path, *src_cloud);
	//PCLUtil_cpp::hoge();
	//std::cout << "Average Curvature : " << PCLUtil_cpp::GetAverageCurvature (src_cloud);
}

void regionGrowingTest(string src_pcd_path){
	boost::filesystem::path src_path(src_pcd_path);
	boost::filesystem::path dirpath = src_path.parent_path();
	boost::filesystem::path basename = src_path.stem(); // filename w/o extention
	boost::filesystem::path abs_path_without_ext = dirpath / basename;
	std::string ext = src_path.extension().string();

	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path, *src_cloud);

	PCn::Ptr normals(new PCn);
	KdT<pcl::PointXYZ>::Ptr kdTreeT(new KdT<pcl::PointXYZ>);
	PCLUtil_cpp::CalcNormal(src_cloud, normals, kdTreeT);

	for (size_t i = 1; i < 5; i++)
	{
		double curv = 0.005 * i;
		for (size_t j = 1; j < 2; j++)
		{
			double ang = 9 * j;
			std::stringstream strm;
			strm << abs_path_without_ext.string() << "_" << ang << "_" << curv << ext;

			PCTPtr<pcl::PointXYZRGB> dst_cloud(new PCxyzrgb);

			PCLUtil_cpp::RegionGrowingClustering(src_cloud, dst_cloud, kdTreeT, normals, 30, ang, curv);
			pcl::io::savePCDFile(strm.str(), *dst_cloud);
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	/*std::string src_pcd_path_str = R"(C:\Users\headl\Desktop\201507151941497331_0.005_Head_ds.pcd)";
	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path_str, *src_cloud);

	PCTPtr<pcl::PointNormal> dst_cloud(new PCxyzn);
	PCLUtil_cpp::CalcNormal(src_cloud, dst_cloud);
	pcl::io::savePCDFileASCII(R"(C:\Users\headl\Desktop\201507151941497331_0.005_Head_ds_n.pcd)", *dst_cloud);
	
	PCTPtr<pcl::PointNormal> dst_cloud2(new PCxyzn);
	PCLUtil_cpp::CalcNormal(src_cloud, dst_cloud2, 2000);
	pcl::io::savePCDFileASCII(R"(C:\Users\headl\Desktop\201507151941497331_0.005_Head_ds_n2.pcd)", *dst_cloud);
*/


	//std::string src_pcd_path_str = R"(C:\Users\headl\Desktop\e.pcd)";

	//PCxyz::Ptr src_cloud(new PCxyz);
	//pcl::io::loadPCDFile(src_pcd_path_str, *src_cloud);

	//PCTPtr<pcl::PointNormal> dst_cloud(new PCxyzn);
	//PCLUtil_cpp::CalcNormal(src_cloud, dst_cloud);

	//PCLUtil_cpp::RegionGrowingClustering(src_cloud, R"(C:\Users\headl\Desktop\f.pcd)");

	//pcl::io::savePCDFileASCII(R"(C:\Users\headl\Desktop\e_n.pcd)", *dst_cloud);


	//regionGrowingTest(src_pcd_path_str);
	/*
	PCxyz::Ptr src_cloud(new PCxyz);
	pcl::io::loadPCDFile(src_pcd_path_str, *src_cloud);

	PCxyz::Ptr dst_cloud(new PCxyz);

	PCLUtil_cpp::RegionGrowingClustering(src_cloud, R"(C:\Users\headl\Desktop\e_3_0.1.pcd)", 30, 3, 0.1);
*/
	//PCLUtil_cpp::RemoveOutlierStatistic(src_cloud, dst_cloud, 30);

	//pcl::io::savePCDFileASCII(R"(C:\Users\headl\Desktop\d.pcd)", *dst_cloud);

	//PCLUtil_cpp::RegionGrowingClustering(src_cloud, R"(C:\Users\headl\Desktop\c_10_0,005.pcd)", 30, 10, 0.005);

	//PCxyzn::Ptr dst_cloud(new PCxyzn);
	//CalcNormal<pcl::PointXYZ, pcl::PointNormal>(src_cloud, dst_cloud);

	//VisualizeCurvature<pcl::PointNormal>(dst_cloud);

	std::getchar();
	return 0;
}
