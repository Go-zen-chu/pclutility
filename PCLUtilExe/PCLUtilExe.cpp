﻿#include "stdafx.h"
#include "PCLUtilExe.h"


namespace PCLUtil_cpp{
#pragma region using

	// using in header is not recommended https://msdn.microsoft.com/ja-jp/library/5cb46ksf.aspx

	//using std::vector;
	//using std::string;

	//using boost::shared_ptr;

	//using pcl::PointCloud;
	//using pcl::visualization::PCLVisualizer;
#pragma endregion

	// for sorting path in dictionary order
	inline bool path_comparator(const fs::path path1, const fs::path path2){
		return path1.string() < path2.string();
	}

	double PCLUtil_cpp::GetAverageCurvature(const PCTPtr<pcl::PointNormal> &src_cloud)
	{
		double ave_curv = 0;
		for each (auto point in src_cloud->points)
		{
			ave_curv += point.curvature;
		}
		ave_curv /= src_cloud->points.size();
		return ave_curv;
	}

	void PCLUtil_cpp::Visualize(const pcl::PolygonMesh polyMesh){
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
		viewer->initCameraParameters();
		viewer->addPolygonMesh(polyMesh);
		while (!viewer->wasStopped())
		{
			viewer->spinOnce(100);
			boost::this_thread::sleep(boost::posix_time::microseconds(100000));
		}
		viewer->close();
	}

#pragma region fusion code

	//void fusionPointCloud(const string src_dir_path_str, const string dst_pcd_path_str, double leafSize, const string ext = "_Head.pcd", bool showInfo = false){
	//	fs::path src_dir_path(src_dir_path_str);
	//	if (fs::exists(src_dir_path) == false){
	//		std::cerr << "Folder " << src_dir_path_str << " doesn't exists" << "\n";
	//		return;
	//	}
	//	shared_ptr<vector<fs::path>> src_path_vector(new vector<fs::path>);
	//	BOOST_FOREACH(const fs::path &filepath, std::make_pair(fs::directory_iterator(src_dir_path), fs::directory_iterator())){
	//		if (fs::is_directory(filepath) == false && boost::algorithm::ends_with(filepath.string(), ext)){
	//			src_path_vector->push_back(filepath);
	//		}
	//	}
	//	std::sort(src_path_vector->begin(), src_path_vector->end(), path_comparator);
	//	PCxyz::Ptr src_cloud(new PCxyz);
	//	PCxyz::Ptr dst_cloud(new PCxyz); // merged and downsampled cloud
	//	PCxyz::Ptr aligned_cloud(new PCxyz);
	//	pcl::PCLPointCloud2::Ptr before_downsample(new pcl::PCLPointCloud2);
	//	pcl::PCLPointCloud2::Ptr after_downsample(new pcl::PCLPointCloud2);
	//	double fitness_score = -1;
	//	pcl::io::loadPCDFile((*src_path_vector)[0].string(), *dst_cloud);
	//	for (size_t i = 1; i < src_path_vector->size(); i++)
	//	{
	//		pcl::io::loadPCDFile((*src_path_vector)[i].string(), *src_cloud); // read new file
	//		iterativeClosestPoint(src_cloud, dst_cloud, aligned_cloud, &fitness_score, showInfo);
	//		// merge cloud
	//		(*dst_cloud) += (*aligned_cloud);
	//		// downsample , need to replace pointcloud2
	//		pcl::toPCLPointCloud2(*dst_cloud, *before_downsample);
	//		//voxelGridDownSample(before_downsample, after_downsample, leafSize, leafSize, leafSize);
	//		pcl::fromPCLPointCloud2(*after_downsample, *dst_cloud);
	//	}
	//	// export pcd
	//	pcl::io::savePCDFile(dst_pcd_path_str, *dst_cloud);
	//}
	//void fusionPointCloudGeneral(const string src_dir_path_str, const string dst_pcd_path_str, double leafSize, const string suffix = "_Head_ds.pcd", bool showInfo = false){
	//	fs::path src_dir_path(src_dir_path_str);
	//	if (fs::exists(src_dir_path) == false){
	//		std::cerr << "Folder " << src_dir_path_str << " doesn't exists" << "\n";
	//		return;
	//	}
	//	shared_ptr<vector<fs::path>> src_path_vector(new vector<fs::path>);
	//	BOOST_FOREACH(const fs::path &filepath, std::make_pair(fs::directory_iterator(src_dir_path), fs::directory_iterator())){
	//		if (fs::is_directory(filepath) == false && boost::algorithm::ends_with(filepath.string(), suffix)){
	//			src_path_vector->push_back(filepath);
	//		}
	//	}
	//	std::sort(src_path_vector->begin(), src_path_vector->end(), path_comparator);
	//	PCxyzn::Ptr src_cloud(new PCxyzn);
	//	PCxyzn::Ptr dst_cloud(new PCxyzn); // merged and downsampled cloud
	//	PCxyzn::Ptr aligned_cloud(new PCxyzn);
	//	double fitness_score = -1;
	//	pcl::io::loadPCDFile((*src_path_vector)[0].string(), *dst_cloud);
	//	for (size_t i = 1; i < src_path_vector->size(); i++)
	//	{
	//		pcl::io::loadPCDFile((*src_path_vector)[i].string(), *src_cloud); // read new file
	//		iterativeClosestPointGeneral(src_cloud, dst_cloud, aligned_cloud, &fitness_score, showInfo);
	//		// merge cloud
	//		(*dst_cloud) += (*aligned_cloud);
	//		voxelGridDownSample(dst_cloud, dst_cloud, leafSize, leafSize, leafSize, showInfo);
	//	}
	//	// export pcd
	//	pcl::io::savePCDFile(dst_pcd_path_str, *dst_cloud);
	//}
	//void fusionPointCloudGeneral(const shared_ptr<vector<string>> src_path_vector, const string dst_pcd_path_str, double leafSize = 0.005, bool showInfo = false){
	//	std::sort(src_path_vector->begin(), src_path_vector->end(), path_comparator);
	//	PCxyzn::Ptr src_cloud(new PCxyzn);
	//	PCxyzn::Ptr dst_cloud(new PCxyzn); // merged and downsampled cloud
	//	PCxyzn::Ptr aligned_cloud(new PCxyzn);
	//	double fitness_score = -1;
	//	pcl::io::loadPCDFile((*src_path_vector)[0], *dst_cloud);
	//	for (size_t i = 1; i < src_path_vector->size(); i++)
	//	{
	//		pcl::io::loadPCDFile((*src_path_vector)[i], *src_cloud); // read new file
	//		//removeOutlierStatistic(src_cloud, src_cloud, 50, showInfo);
	//		iterativeClosestPointGeneral(src_cloud, dst_cloud, aligned_cloud, &fitness_score, showInfo);
	//		if (fitness_score > 0.0001) continue;
	//		// merge cloud
	//		(*aligned_cloud) += (*dst_cloud);
	//		voxelGridDownSample(aligned_cloud, dst_cloud, leafSize, leafSize, leafSize, showInfo);
	//	}
	//	// export pcd
	//	pcl::io::savePCDFile(dst_pcd_path_str, *dst_cloud);
	//}
	//void fusionPointCloudGeneral(const shared_ptr<vector<string>> src_path_vector, const shared_ptr<vector<double>> x_vector, const shared_ptr<vector<double>> y_vector, const string dst_pcd_path_str, double leafSize = 0.005, bool showInfo = false){
	//	std::sort(src_path_vector->begin(), src_path_vector->end(), path_comparator);
	//	PCxyzn::Ptr src_cloud(new PCxyzn);
	//	PCxyzn::Ptr dst_cloud(new PCxyzn); // merged and downsampled cloud
	//	PCxyzn::Ptr aligned_cloud(new PCxyzn);
	//	double fitness_score = -1;
	//	pcl::io::loadPCDFile((*src_path_vector)[0], *dst_cloud);
	//	double baseX = (*x_vector)[0];
	//	double baseY = (*y_vector)[0];
	//	for (size_t i = 1; i < src_path_vector->size(); i++)
	//	{
	//		pcl::io::loadPCDFile((*src_path_vector)[i], *src_cloud); // read new file
	//		double srcX = (*x_vector)[i];
	//		double srcY = (*y_vector)[i];
	//		movePointCloud(src_cloud, src_cloud, baseX - srcX, baseY - srcY, 0);
	//		//removeOutlierStatistic(src_cloud, src_cloud, 50, showInfo);
	//		iterativeClosestPointGeneral(src_cloud, dst_cloud, aligned_cloud, &fitness_score, showInfo);
	//		if (fitness_score > 0.0001) continue;
	//		// merge cloud
	//		(*aligned_cloud) += (*dst_cloud);
	//		voxelGridDownSample(aligned_cloud, dst_cloud, leafSize, leafSize, leafSize, showInfo);
	//	}
	//	// export pcd
	//	pcl::io::savePCDFile(dst_pcd_path_str, *dst_cloud);
	//}

	//void fusionPointCloudRand(const string src_dir_path_str, const string dst_pcd_path_str, double leafSize, const string ext = "_Head.pcd", bool showInfo = false){
	//	fs::path src_dir_path(src_dir_path_str);
	//	if (fs::exists(src_dir_path) == false){
	//		std::cerr << "Folder " << src_dir_path_str << " doesn't exists" << "\n";
	//		return;
	//	}
	//	shared_ptr<vector<fs::path>> src_path_vector(new vector<fs::path>);
	//	std::uniform_real_distribution<> rnd1(0, 1);
	//	std::mt19937 mt(1); // Mersenne twister
	//	BOOST_FOREACH(const fs::path &filepath, std::make_pair(fs::directory_iterator(src_dir_path), fs::directory_iterator())){
	//		if (fs::is_directory(filepath) == false && boost::algorithm::ends_with(filepath.string(), ext) && rnd1(mt) > 0.7){
	//			src_path_vector->push_back(filepath);
	//		}
	//	}
	//	std::sort(src_path_vector->begin(), src_path_vector->end(), path_comparator);
	//	PCxyz::Ptr src_cloud(new PCxyz);
	//	PCxyz::Ptr dst_cloud(new PCxyz); // merged and downsampled cloud
	//	PCxyz::Ptr aligned_cloud(new PCxyz);
	//	pcl::PCLPointCloud2::Ptr before_downsample(new pcl::PCLPointCloud2);
	//	pcl::PCLPointCloud2::Ptr after_downsample(new pcl::PCLPointCloud2);
	//	double fitness_score = -1;
	//	pcl::io::loadPCDFile((*src_path_vector)[0].string(), *dst_cloud);
	//	for (size_t i = 1; i < src_path_vector->size(); i++)
	//	{
	//		pcl::io::loadPCDFile((*src_path_vector)[i].string(), *src_cloud); // read new file
	//		iterativeClosestPoint(src_cloud, dst_cloud, aligned_cloud, &fitness_score, showInfo);
	//		// merge cloud
	//		(*dst_cloud) += (*aligned_cloud);
	//		// downsample , need to replace pointcloud2
	//		pcl::toPCLPointCloud2(*dst_cloud, *before_downsample);
	//		//voxelGridDownSample(before_downsample, after_downsample, leafSize, leafSize, leafSize);
	//		pcl::fromPCLPointCloud2(*after_downsample, *dst_cloud);
	//	}
	//	// export pcd
	//	pcl::io::savePCDFile(dst_pcd_path_str, *dst_cloud);
	//}

#pragma endregion

#pragma region command functions

	void PCLUtil_cpp::FilterPointCloud_XYZ(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		std::string dst_path = (*argList)[1];
		std::string field_name = (*argList)[2];
		double limit_min = atof((*argList)[3].c_str());
		double limit_max = atof((*argList)[4].c_str());

		pcl::PointCloud<pcl::PointXYZ>::Ptr src_cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		pcl::PointCloud<pcl::PointXYZ>::Ptr dst_cloud(new pcl::PointCloud<pcl::PointXYZ>);

		PCLUtil_cpp::FilterPointCloud(src_cloud, dst_cloud, field_name, limit_min, limit_max);
		pcl::io::savePCDFileASCII(dst_path, *dst_cloud);
	}

	void PCLUtil_cpp::VoxelGridDownSample_XYZ(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		std::string dst_path = (*argList)[1];
		double leafSize = atof((*argList)[2].c_str());

		pcl::PointCloud<pcl::PointXYZ>::Ptr src_cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		pcl::PointCloud<pcl::PointXYZ>::Ptr dst_cloud(new pcl::PointCloud<pcl::PointXYZ>);
		VoxelGridDownSample(src_cloud, dst_cloud, leafSize);
		pcl::io::savePCDFileASCII(dst_path, *dst_cloud);
	}

	void PCLUtil_cpp::SmoothPointCloud_XYZ_XYZN(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		std::string dst_path = (*argList)[1];
		double radius = atof((*argList)[2].c_str());
		
		pcl::PointCloud<pcl::PointXYZ>::Ptr src_cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		pcl::PointCloud<pcl::PointNormal>::Ptr dst_cloud(new pcl::PointCloud<pcl::PointNormal>);
		SmoothPointCloud(src_cloud, dst_cloud, radius);
		pcl::io::savePCDFileASCII(dst_path, *dst_cloud);
	}

	void PCLUtil_cpp::SmoothPointCloud_XYZN_XYZN(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		std::string dst_path = (*argList)[1];
		double radius = atof((*argList)[2].c_str());

		// load PointNormal data in PointXYZ format because of pcl does not support  PointNormal -> PointNormal
		pcl::PointCloud<pcl::PointXYZ>::Ptr src_cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		pcl::PointCloud<pcl::PointNormal>::Ptr dst_cloud(new pcl::PointCloud<pcl::PointNormal>);
		SmoothPointCloudNormal(src_cloud, dst_cloud, radius);
		pcl::io::savePCDFileASCII(dst_path, *dst_cloud);
	}
	
	double PCLUtil_cpp::CalcSurfaceArea_XYZN(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		double radius = atof((*argList)[1].c_str());
		double mu = atof((*argList)[1].c_str());

		pcl::PointCloud<pcl::PointNormal>::Ptr src_cloud(new pcl::PointCloud<pcl::PointNormal>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		return CalcSurfaceArea(src_cloud, radius, mu);
	}

	double PCLUtil_cpp::GetAverageCurvature_XYZN(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		pcl::PointCloud<pcl::PointNormal>::Ptr src_cloud(new pcl::PointCloud<pcl::PointNormal>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		return GetAverageCurvature(src_cloud);
	}

	double PCLUtil_cpp::IterativeClosestPoint_Gen_XYZN(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		std::string dst_path = (*argList)[1];
		double dx = atof((*argList)[2].c_str());
		double dy = atof((*argList)[3].c_str());
		double dz = atof((*argList)[4].c_str());

		pcl::PointCloud<pcl::PointNormal>::Ptr src_cloud(new pcl::PointCloud<pcl::PointNormal>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		pcl::PointCloud<pcl::PointNormal>::Ptr dst_cloud(new pcl::PointCloud<pcl::PointNormal>);
		pcl::io::loadPCDFile(dst_path, *dst_cloud);

		MovePointCloud(src_cloud, src_cloud, dx, dy, dz);

		pcl::PointCloud<pcl::PointNormal>::Ptr aligned_cloud(new pcl::PointCloud<pcl::PointNormal>);
		double fitness_score = -1;
		IterativeClosestPoint_Gen(src_cloud, dst_cloud, aligned_cloud, &fitness_score);
		return fitness_score;
	}

	void PCLUtil_cpp::RegionGrowingClustering_XYZ(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];
		std::string dst_path = (*argList)[1];
		int ksearch = atoi((*argList)[2].c_str());
		double smooth_angle = atof((*argList)[3].c_str());
		double curv_thres = atof((*argList)[4].c_str());

		pcl::PointCloud<pcl::PointXYZ>::Ptr src_cloud(new pcl::PointCloud<pcl::PointXYZ>);
		pcl::io::loadPCDFile(src_path, *src_cloud);
		PCLUtil_cpp::RegionGrowingClustering(src_cloud, dst_path, ksearch, smooth_angle, curv_thres);
	}

	void PCLUtil_cpp::Visualize_XYZ(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];

		Visualize<pcl::PointXYZ>(src_path);
	}

	void PCLUtil_cpp::Visualize_XYZN(boost::shared_ptr<std::vector<std::string>> argList){
		std::string src_path = (*argList)[0];

		Visualize<pcl::PointNormal>(src_path);
	}

#pragma endregion

}

int main(int argc, char **argv)
{
	// arg storing list
	boost::shared_ptr<std::vector<std::string>> argList(new std::vector<std::string>);
	std::string commandFlag = "";
	// parse argments
	for (size_t i = 1; i < argc; i++) // argc = num argments
	{
		if (commandFlag == "" && argv[i][0] == '-') // be aware of minus value for numeric args
			commandFlag = argv[i]; // command
		else
			argList->push_back(argv[i]); //value
	}

	// switching string is not easy in C++
	if (commandFlag == ""){
		throw "NoCommandException : Missing command\n"; return -1;
	}
	else if (commandFlag == "-fpc_xyz")				PCLUtil_cpp::FilterPointCloud_XYZ(argList);
	else if (commandFlag == "-vgds_xyz")		PCLUtil_cpp::VoxelGridDownSample_XYZ(argList);
	else if (commandFlag == "-spc_xyz_xyzn")	PCLUtil_cpp::SmoothPointCloud_XYZ_XYZN(argList);
	else if (commandFlag == "-spc_xyzn_xyzn")	PCLUtil_cpp::SmoothPointCloud_XYZN_XYZN(argList);
	else if (commandFlag == "-csa_xyzn")		std::cout << PCLUtil_cpp::CalcSurfaceArea_XYZN(argList);
	else if (commandFlag == "-gac_xyzn")		std::cout << PCLUtil_cpp::GetAverageCurvature_XYZN(argList);
	else if (commandFlag == "-icpg_xyzn")		std::cout << PCLUtil_cpp::IterativeClosestPoint_Gen_XYZN(argList);
	else if (commandFlag == "-rgc_xyz")			PCLUtil_cpp::RegionGrowingClustering_XYZ(argList);
	else if (commandFlag == "-v_xyz")			PCLUtil_cpp::Visualize_XYZ(argList);
	else if (commandFlag == "-v_xyzn")			PCLUtil_cpp::Visualize_XYZN(argList);
	else
	{
		throw "CommandNotDefinedException : No such command\n"; return -1;
	}
	return 0;
}