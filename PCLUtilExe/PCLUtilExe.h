﻿#pragma once

namespace PCLUtil_cpp{

#pragma region typedef
	typedef pcl::PointCloud<pcl::PointXYZ> PCxyz; // Cloud of xyz
	typedef pcl::PointCloud<pcl::PointNormal> PCxyzn; // Cloud of xyz & normal
	typedef pcl::PointCloud<pcl::Normal> PCn; // Cloud of normal
	typedef pcl::PointCloud<pcl::PointXYZRGB> PCxyzrgb; // Cloud of rgb
	typedef pcl::PointCloud<pcl::PointXYZRGBA> PCxyzrgba; // Cloud of rgba

	template <class PointT>
	using  PCT = pcl::PointCloud<PointT>;
	template <class PointT>
	using  PCTPtr = boost::shared_ptr< pcl::PointCloud<PointT> >;

	// type definition for template
	template <class PointT>
	using  KdT = pcl::search::KdTree<PointT>;

	template <class PointT>
	using  KdTPtr = boost::shared_ptr<pcl::search::KdTree<PointT>>;

	template <class PointT>
	using PCColorT = pcl::visualization::PointCloudColorHandlerCustom<PointT>;

	// not sure this is allowed in header...
	//template <class PointT> PCColorT<PointT> red(255, 0, 0);
	//template <class PointT> PCColorT<PointT> green(0, 255, 0);
	//template <class PointT> PCColorT<PointT> blue(255, 0, 0);
	//template <class PointT> PCColorT<PointT> yellow(255, 255, 0);
	//template <class PointT> PCColorT<PointT> purple(255, 0, 255);
	//template <class PointT> PCColorT<PointT> lightBlue(0, 255, 255);


	namespace fs = boost::filesystem;

#pragma endregion

#pragma region Basic Functions

	template <class PointT>
	void FilterPointCloud(const PCTPtr<PointT> &src_cloud, PCTPtr<PointT> &dst_cloud, const std::string field_name, double limit_min, double limit_max){
		pcl::PassThrough<PointT> pass;
		pass.setInputCloud(src_cloud);
		pass.setFilterFieldName(field_name);
		pass.setFilterLimits(limit_min, limit_max);
		pass.filter(*dst_cloud);
	}

	template <class PointT>
	void MovePointCloud(const PCTPtr<PointT> &src_cloud, PCTPtr<PointT> &dst_cloud, double dx, double dy, double dz){
		Eigen::Matrix4f translate_matrix;
		translate_matrix << \
			1, 0, 0, dx, \
			0, 1, 0, dy, \
			0, 0, 1, dz, \
			0, 0, 0, 1;
		pcl::transformPointCloud(*src_cloud, *dst_cloud, translate_matrix);
	}

#pragma endregion

#pragma region Normal Calculation
	// Just calculate normal and return as normals, kdtree
	template <class PointT>
	void CalcNormal(const PCTPtr<PointT> &src_cloud, PCn::Ptr &normals, KdTPtr< PointT > &kdTreeT, int ksearch = 30){
		pcl::NormalEstimation<PointT, pcl::Normal> norm_est;
		norm_est.setInputCloud(src_cloud);
		norm_est.setSearchMethod(kdTreeT);
		norm_est.setKSearch(ksearch);
		norm_est.compute(*normals);
	}

	// Add normal to PointInT and return as PointOutT
	template <class PointInT, class PointOutT>
	void CalcNormal(const PCTPtr<PointInT> &src_cloud, PCTPtr<PointOutT> &dst_cloud, int ksearch = 30) {
		PCn::Ptr normals(new PCn);
		KdT<PointInT>::Ptr kdTreeT(new KdT<PointInT>);
		CalcNormal(src_cloud, normals, kdTreeT, ksearch);
		pcl::concatenateFields(*src_cloud, *normals, *dst_cloud);
	}

	template <class PointT>
	void CalcNormalRadius(const PCTPtr<PointT> &src_cloud, PCn::Ptr &normals, KdTPtr< PointT > &kdTreeT, double radius = 0.1){
		pcl::NormalEstimation<PointT, pcl::Normal> norm_est;
		norm_est.setInputCloud(src_cloud);
		norm_est.setSearchMethod(kdTreeT);
		norm_est.setRadiusSearch(radius);
		norm_est.compute(*normals);
	}

	double GetAverageCurvature(const PCTPtr<pcl::PointNormal> &src_cloud);

#pragma endregion

#pragma region downsample & smoothing
	template <class PointT>
	void VoxelGridDownSample(const PCTPtr<PointT> &src_cloud, PCTPtr<PointT> &dst_cloud, double leafSize){
		VoxelGridDownSample(src_cloud, dst_cloud, leafSize, leafSize, leafSize);
	}

	template <class PointT>
	void VoxelGridDownSample(const PCTPtr<PointT> &src_cloud, PCTPtr<PointT> &dst_cloud, double leafX, double leafY, double leafZ){
		// Create the filtering object
		pcl::VoxelGrid<PointT> sor;
		sor.setInputCloud(src_cloud);
		sor.setLeafSize(leafX, leafY, leafZ);
		sor.filter(*dst_cloud);
	}

	// remove outlier points using statistical filter. this function does not down sample point cloud.
	template <class PointT>
	void RemoveOutlierStatistic(const PCTPtr<PointT> &src_cloud, PCTPtr<PointT> &dst_cloud, int ksearch = 50, double stdDevThres = 1.0){
		pcl::StatisticalOutlierRemoval<PointT> sor;
		sor.setInputCloud(src_cloud);
		sor.setMeanK(ksearch);
		sor.setStddevMulThresh(stdDevThres);
		sor.filter(*dst_cloud);
	}

	template <class PointInT, class PointOutT>
	void SmoothPointCloud(const PCTPtr<PointInT> &src_cloud, PCTPtr<PointOutT> &dst_cloud, double radius = 0.1){
		// Create a KD-Tree
		KdT<PointInT>::Ptr kdTreeT(new KdT<PointInT>);
		// Init object (second point type is for the normals, even if unused)
		pcl::MovingLeastSquares<PointInT, PointOutT> mls;
		mls.setComputeNormals(true);

		mls.setInputCloud(src_cloud);
		mls.setPolynomialFit(true);
		mls.setSearchMethod(kdTreeT);
		mls.setSearchRadius(radius);

		mls.process(*dst_cloud);
	}

	// this function doesn't calculate normal of src_cloud
	// causing link error may be because pcl::MovingLeastSquares<PointXYZ, PointNormal> is only supported
	// http://www.pcl-users.org/Moving-Least-Squares-with-color-data-td3954666.html

	//template <class PointNormalT>
	//void SmoothPointCloudNormal(const PCTPtr<PointNormalT> &src_cloud, PCTPtr<PointNormalT> &dst_cloud, double radius = 0.1){
	//	// Create a KD-Tree
	//	KdT<PointNormalT>::Ptr kdTreeT(new KdT<PointNormalT>);
	//	// Init object (second point type is for the normals, even if unused)
	//	pcl::MovingLeastSquares <PointNormalT, PointNormalT> mls;
	//	mls.setComputeNormals(false); // don't calculate normal that has already been calculated

	//	mls.setInputCloud(src_cloud);
	//	mls.setPolynomialFit(true);
	//	mls.setSearchMethod(kdTreeT);
	//	mls.setSearchRadius(radius);

	//	mls.process(*dst_cloud);
	//}

	template <class PointOutT>
	void SmoothPointCloudNormal(const PCTPtr<pcl::PointXYZ> &src_cloud, PCTPtr<PointOutT> &dst_cloud, double radius = 0.1){
		// Create a KD-Tree
		KdT<pcl::PointXYZ>::Ptr kdTreeT(new KdT<pcl::PointXYZ>);
		// Init object (second point type is for the normals, even if unused)
		pcl::MovingLeastSquares <pcl::PointXYZ, PointOutT> mls;
		mls.setComputeNormals(false); // don't calculate normal that has already been calculated

		mls.setInputCloud(src_cloud);
		mls.setPolynomialFit(true);
		mls.setSearchMethod(kdTreeT);
		mls.setSearchRadius(radius);

		mls.process(*dst_cloud);
	}
#pragma endregion

#pragma region triangulation

	template <class PointT>
	double SignedVolumeOfTriangle(const PointT p1, const  PointT p2, const  PointT p3)
	{
		Eigen::Vector3f p2p1(p2.x - p1.x, p2.y - p1.y, p2.z - p1.z);
		Eigen::Vector3f p3p1(p3.x - p1.x, p3.y - p1.y, p3.z - p1.z);
		return p2p1.cross(p3p1).norm() / 2;
	}

	template <class PointT>
	double VolumeOfMesh(const pcl::PolygonMesh mesh)
	{
		double vols = 0.0;
		pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
		pcl::fromPCLPointCloud2(mesh.cloud, *cloud);
		for (int triangle = 0; triangle < mesh.polygons.size(); triangle++)
		{
			PointT pt1 = cloud->points[mesh.polygons[triangle].vertices[0]];
			PointT pt2 = cloud->points[mesh.polygons[triangle].vertices[1]];
			PointT pt3 = cloud->points[mesh.polygons[triangle].vertices[2]];
			vols += SignedVolumeOfTriangle(pt1, pt2, pt3);
		}
		return std::abs(vols);
	}

	template <class PointNormalT>
	void GenerateMesh(const PCTPtr<PointNormalT> &src_cloud, pcl::PolygonMesh &triangles, double radius = 0.2, double mu = 2.5){
		KdT<PointNormalT>::Ptr kdTreeT(new KdT<PointNormalT>);
		kdTreeT->setInputCloud(src_cloud);

		pcl::GreedyProjectionTriangulation<PointNormalT> gp3;

		// Set the maximum distance between connected points (maximum edge length)
		gp3.setSearchRadius(radius);
		gp3.setMu(mu); // Set the multiplier of the nearest neighbor distance
		gp3.setMaximumNearestNeighbors(100); //Set the maximum number of nearest neighbors to be searched for.

		// gp3.setMaximumSurfaceAngle(M_PI / 4); // 45 degrees
		// gp3.setMinimumAngle(M_PI / 18); // 10 degrees
		// gp3.setMaximumAngle(2 * M_PI / 3); // 120 degrees
		gp3.setNormalConsistency(true);

		//pcl::PolygonMesh triangles;
		// Get result
		gp3.setInputCloud(src_cloud);
		gp3.setSearchMethod(kdTreeT);
		gp3.reconstruct(triangles);
	}

	template <class PointNormalT>
	double CalcSurfaceArea(const PCTPtr<PointNormalT> &src_cloud, double radius = 0.2, double mu = 2.5){
		pcl::PolygonMesh triangles;
		GenerateMesh(src_cloud, triangles, radius, mu);
		return VolumeOfMesh<PointNormalT>(triangles);
	}

#pragma endregion

#pragma region ICP

	template<class PointT>
	void IterativeClosestPoint(const PCTPtr<PointT> &src_cloud, const PCTPtr<PointT> &dst_cloud, PCTPtr<PointT> &aligned_cloud, double *fitness_score){
		pcl::IterativeClosestPoint<PointT, PointT> icp;
		icp.setMaxCorrespondenceDistance(0.01); // 1cm 
		icp.setMaximumIterations(500);
		icp.setInputSource(src_cloud);
		icp.setInputTarget(dst_cloud);
		icp.align(*aligned_cloud);

		*fitness_score = icp.getFitnessScore();
	}

	template<class PointT>
	void IterativeClosestPoint_Gen(const PCTPtr<PointT> &src_cloud, const PCTPtr<PointT> &dst_cloud, PCTPtr<PointT> &aligned_cloud, double *fitness_score){
		pcl::GeneralizedIterativeClosestPoint<PointT, PointT> gicp;
		//gicp.setMaxCorrespondenceDistance(0.01); // 1cm
		gicp.setMaximumIterations(500);
		gicp.setInputSource(src_cloud);
		gicp.setInputTarget(dst_cloud);
		gicp.align(*aligned_cloud);
		*fitness_score = gicp.getFitnessScore();
	}

	//template<typename PointT>
	//double[] Averaged_GICP(const PCTPtr<PointT> &src_cloud, const PCTPtr<PointT> &dst_cloud){
	//	boost::shared_ptr<pcl::PointCloud<PointT>> aligned_cloud(new pcl::PointCloud<PointT>);
	//	double fitness12 = -1;
	//	IterativeClosestPoint_Gen(cloud1, cloud2, aligned_cloud, &fitness12);
	//	double fitness21 = -1;
	//	IterativeClosestPoint_Gen(cloud2, cloud1, aligned_cloud, &fitness21);
	//	double[] fitnessScore = new double[2] { fitness12, fitness21 };
	//	return fitnessScore;
	//}

#pragma endregion

#pragma region Region Growing / Segmentation

	template <class PointT>
	void SavePointCloudCluster(const PCTPtr<PointT> &src_cloud, const std::vector<pcl::PointIndices> &cluster_indices, const std::string &dst_path){
		fs::path dirpath = dst_path.parent_path();
		fs::path basename = dst_path.stem(); // filename w/o extention
		fs::path abs_path_without_ext = dirpath / basename;

		int cluster_idx = 0;
		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); it++, cluster_idx++)
		{
			PCTPtr<PointT> src_cloud_cls(new PCT<PointT>);
			for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); ++pit)
			{
				src_cloud_cls->points.push_back(src_cloud->points[*pit]); // pick points
			}
			src_cloud_cls->width = int(src_cloud_cls->points.size());
			src_cloud_cls->height = 1;
			src_cloud_cls->is_dense = true;

			//Save cluster
			cout << "[SavePointCloudCluster] PointCloud representing the Cluster: " << src_cloud_cls->points.size() << " data points." << std::endl;
			std::stringstream ss;
			ss << abs_path_without_ext.string() << "_" << cluster_idx << ".pcd";
			pcl::io::savePCDFileASCII(ss.str(), *src_cloud_cls);
		}
	}

	// you can get similar code in region_growing.hpp getColoredCloudRGBA()
	template <class PointT>
	PCxyzrgb::Ptr PaintPointCloudCluster(const PCTPtr<PointT> &src_cloud, const std::vector<pcl::PointIndices> &cluster_indices){
		PCxyzrgb::Ptr dst_cloud(new PCxyzrgb);
		// when you copied points all colors are painted 0
		//pcl::copyPointCloud(*src_cloud, *dst_cloud); 

		// so you need to paint them
		dst_cloud->width = src_cloud->width;
		dst_cloud->height = src_cloud->height;
		dst_cloud->is_dense = src_cloud->is_dense;
		for (size_t i_point = 0; i_point < src_cloud->points.size(); i_point++)
		{
			pcl::PointXYZRGB point;
			point.x = *(src_cloud->points[i_point].data);
			point.y = *(src_cloud->points[i_point].data + 1);
			point.z = *(src_cloud->points[i_point].data + 2);
			point.r = 255;
			point.g = 255;
			point.b = 255;
			dst_cloud->points.push_back(point);
		}

		// create random color
		std::random_device rnddv;
		std::mt19937 mt(rnddv());
		std::vector<unsigned char> colors;
		for (size_t cls_idx = 0; cls_idx < cluster_indices.size(); cls_idx++)
		{
			colors.push_back(static_cast<unsigned char> (mt() % 256));
			colors.push_back(static_cast<unsigned char> (mt() % 256));
			colors.push_back(static_cast<unsigned char> (mt() % 256));
			cout << mt() % 256;
		}

		int cls_idx = 0;
		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin(); it != cluster_indices.end(); it++, cls_idx++)
		{
			for (std::vector<int>::const_iterator pit = it->indices.begin(); pit != it->indices.end(); pit++)
			{
				dst_cloud->points[*pit].r = colors[3 * cls_idx]; // set random color
				dst_cloud->points[*pit].g = colors[3 * cls_idx + 1]; // set random color
				dst_cloud->points[*pit].b = colors[3 * cls_idx + 2]; // set random color
			}
		}
		return dst_cloud;
	}

	// region growing without normal calculation
	template <class PointT>
	void RegionGrowingClustering(const PCTPtr<PointT> &src_cloud, PCTPtr<pcl::PointXYZRGB> &dst_cloud, const KdTPtr<PointT> &kdTreeT, const PCn::Ptr &normals, int ksearch = 30, double smooth_angle = 5, double curv_thres = 0.001){
		pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
		reg.setMinClusterSize(30);
		//reg.setMaxClusterSize(1000000);
		reg.setSearchMethod(kdTreeT);
		reg.setNumberOfNeighbours(ksearch);
		reg.setInputCloud(src_cloud);
		//reg.setIndices (indices);
		reg.setInputNormals(normals);

		// The angle in radians that will be used as the allowable range for the normals deviation.
		// If the deviation between points normals is less than smoothness threshold then they are suggested to be in the same cluster (new point - the tested one - will be added to the cluster)
		reg.setSmoothnessThreshold(smooth_angle / 180.0 * M_PI);
		//  If two points have a small normals deviation then the disparity between their curvatures is tested.
		// And if this value is less than curvature threshold then the algorithm will continue the growth of the cluster using new added point.
		reg.setCurvatureThreshold(curv_thres);

		std::vector <pcl::PointIndices> cluster_indices;
		reg.extract(cluster_indices); // you can access to each cluster by using index e,g, clusters[0] contains indice of cluster0's points

		dst_cloud = PaintPointCloudCluster(src_cloud, cluster_indices);
		//dst_cloud = reg.getColoredCloudRGBA();
	}
	
	// wrapping normal calculation code
	template <class PointT>
	void RegionGrowingClustering(const PCTPtr<PointT> &src_cloud, PCTPtr<pcl::PointXYZRGB> &dst_cloud, int ksearch = 30, double smooth_angle = 5, double curv_thres = 0.001){
		PCn::Ptr normals(new PCn);
		KdT<PointT>::Ptr kdTreeT(new KdT<PointT>);
		CalcNormal(src_cloud, normals, kdTreeT, ksearch);

		RegionGrowingClustering(src_cloud, dst_cloud, kdTreeT, normals, ksearch, smooth_angle, curv_thres);
	}

	template <class PointT>
	void RegionGrowingClustering(const PCTPtr<PointT> &src_cloud, std::string dst_path, int ksearch = 30, double smooth_angle = 5, double curv_thres = 0.001) {
		PCxyzrgb::Ptr dst_cloud(new PCxyzrgb);
		RegionGrowingClustering(src_cloud, dst_cloud, ksearch, smooth_angle, curv_thres);
		//Visualize(dst_cloud);
		pcl::io::savePCDFile(dst_path, *dst_cloud);
	}

	template <class PointT>
	std::vector<pcl::PointIndices> EuclideanClusterExtraction(const  PCTPtr<PointT> &src_cloud, const KdTPtr<PointT> &kdTreeT, double radius, double minClusterNum = 30, double maxClusterNum = 100000){
		std::vector<pcl::PointIndices> cluster_indices;
		pcl::EuclideanClusterExtraction<PointT> ec;
		ec.setClusterTolerance(radius);
		ec.setMinClusterSize(minClusterNum);
		ec.setMaxClusterSize(maxClusterNum);
		ec.setSearchMethod(kdTreeT);
		ec.setInputCloud(src_cloud);
		ec.extract(cluster_indices);
		return cluster_indices;
	}

	template <class PointT>
	void DifferenceOfNormals(const  PCTPtr<PointT> &src_cloud, PCxyzn::Ptr &dst_cloud, const PCxyzn::Ptr &small_r_pc, const PCxyzn::Ptr &large_r_pc){
		pcl::DifferenceOfNormalsEstimation<PointT, pcl::PointNormal, pcl::PointNormal> don;
		don.setInputCloud(src_cloud);
		don.setNormalScaleLarge(large_r_pc);
		don.setNormalScaleSmall(small_r_pc);

		PCxyzn::Ptr don_cloud(new PCxyzn);
		// Compute DoN
		don.computeFeature(*don_cloud);
	}

	template <class PointT>
	void DifferenceOfNormalsSegmentation(const PCTPtr<PointT> &src_cloud, std::string don_feature_path, double small_r = 0.05, double large_r = 0.2, double magnitude_th = 2, double seg_r = 0.1){
		PCn::Ptr normals(new PCn);
		KdT<PointInT>::Ptr kdTreeT(new KdT<PointInT>);

		cout << "[DoN]calc normal in small radius" << std::endl;
		PCxyzn::Ptr small_r_pc(new PCxyzn);
		CalcNormalRadius(src_cloud, normals, kdTreeT, small_r);
		pcl::concatenateFields(*src_cloud, *normals, *small_r_pc);
		
		cout << "[DoN]calc normal in large radius" << std::endl;
		PCxyzn::Ptr large_r_pc(new PCxyzn);
		CalcNormalRadius(src_cloud, normals, kdTreeT, large_r);
		pcl::concatenateFields(*src_cloud, *normals, *large_r_pc);

		PCxyzn::Ptr don_cloud(new PCxyzn);
		DifferenceOfNormals(src_cloud, don_cloud, small_r_pc, large_r_pc);
		pcl::io::savePCDFileASCII(don_feature_path, *don_cloud);

		cout << "[DoN]filter by magnitude" << std::endl;
		// Build the condition for filtering
		pcl::ConditionOr<pcl::PointNormal>::Ptr range_cond(new pcl::ConditionOr<pcl::PointNormal>());
		range_cond->addComparison(pcl::FieldComparison<pcl::PointNormal>::ConstPtr(new pcl::FieldComparison<pcl::PointNormal>("curvature", pcl::ComparisonOps::GT, magnitude_th)));
		pcl::ConditionalRemoval<pcl::PointNormal> cond_rem(range_cond);
		cond_rem.setInputCloud(don_cloud);

		PCxyzn::Ptr don_cloud_filtered(new PCxyzn);
		// Apply filter
		cond_rem.filter(*don_cloud_filtered);
		// Save filtered output
		std::cout << "[DoN]Filtered Pointcloud: " << don_cloud_filtered->points.size() << " data points." << std::endl;
		pcl::io::savePCDFileASCII(don_feature_path + "_filtered", *don_cloud_filtered);

		cout << "[DoN]segmentation" << std::endl;
		KdTPtr<pcl::PointNormal> segtree(new KdT<pcl::PointNormal>);
		segtree->setInputCloud(don_cloud_filtered);
		std::vector<pcl::PointIndices> cluster_indices = EuclideanClusterExtraction(don_cloud_filtered, segtree, seg_r);

		PCxyzrgb::Ptr dst_cloud = PaintPointCloudCluster(don_cloud_filtered, don_cloud_filtered);
		pcl::io::savePCDFileASCII(don_feature_path + "_result", *dst_cloud);
	}

#pragma endregion

#pragma region Visualization
	//TODO: It really does not export this func to dll
	void Visualize(const pcl::PolygonMesh polyMesh);

	// Visualize single point cloud
	template <class PointT>
	void Visualize(const PCTPtr<PointT> &src_cloud){
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
		viewer->initCameraParameters();
		viewer->addPointCloud<PointT>(src_cloud);
		while (!viewer->wasStopped())
		{
			viewer->spinOnce(100);
			boost::this_thread::sleep(boost::posix_time::microseconds(100000));
		}
		viewer->close();
	}

	template <class PointT>
	void Visualize(const std::string pcd_path){
		pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
		pcl::io::loadPCDFile(pcd_path, *cloud);
		Visualize<PointT>(cloud);
	}

	template <class PointT>
	void Visualize(const std::vector<std::string> pcd_paths){
		boost::shared_ptr<PCLVisualizer> viewer(new PCLVisualizer("3D Viewer"));
		viewer->initCameraParameters();

		PCColorT<PointT> red(255, 0, 0);
		PCColorT<PointT> green(0, 255, 0);
		PCColorT<PointT> blue(255, 0, 0);
		PCColorT<PointT> yellow(255, 255, 0);
		PCColorT<PointT> purple(255, 0, 255);
		PCColorT<PointT> lightBlue(0, 255, 255);
		boost::shared_ptr<vector<PCColorT<PointT>>> colorDict(new vector < PCColorT<PointT> > { red, green, blue, yellow, purple, lightBlue });

		for (int i = 0; i < pcd_paths.size(); i++)
		{
			int colorIdx = i % 6;
			pcl::PointCloud<PointT>::Ptr cloud(new pcl::PointCloud<PointT>);
			pcl::io::loadPCDFile(pcd_paths[i], *cloud);
			(*colorDict)[colorIdx].setInputCloud(cloud);
			viewer->addPointCloud(cloud, (*colorDict)[colorIdx], "cloud_" + i);
		}
		while (!viewer->wasStopped())
		{
			viewer->spinOnce(100);
			boost::this_thread::sleep(boost::posix_time::microseconds(100000));
		}
		viewer->close();
	}

	template <class PointT>
	void VisualizeCurvature(const PCTPtr<PointT> &src_cloud){
		boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
		viewer->initCameraParameters();

		// min is red and max is blue (i don't know why the api is visualizing like this). confirmed in z axis.
		pcl::visualization::PointCloudColorHandlerGenericField<PointT> curvHandler(src_cloud, "curvature");
		viewer->addPointCloud<PointT>(src_cloud, curvHandler);
		viewer->addCoordinateSystem(0.1); // show axis

		while (!viewer->wasStopped())
		{
			viewer->spinOnce(100);
			boost::this_thread::sleep(boost::posix_time::microseconds(100000));
		}
		viewer->close();
	}

	template <class PointT>
	void VisualizeCurvature(const std::string pcd_path){
		PCTPtr<PointT> src_cloud(new pcl::PointCloud<PointT>);
		pcl::io::loadPCDFile(pcd_path, *src_cloud);
		VisualizeCurvature(src_cloud);
	}

#pragma endregion
	
#pragma region command functions
	void FilterPointCloud_XYZ(boost::shared_ptr<std::vector<std::string>> argList);
	void VoxelGridDownSample_XYZ(boost::shared_ptr<std::vector<std::string>> argList);
	void SmoothPointCloud_XYZ_XYZN(boost::shared_ptr<std::vector<std::string>> argList);
	void SmoothPointCloud_XYZN_XYZN(boost::shared_ptr<std::vector<std::string>> argList);
	double CalcSurfaceArea_XYZN(boost::shared_ptr<std::vector<std::string>> argList);
	double GetAverageCurvature_XYZN(boost::shared_ptr<std::vector<std::string>> argList);
	double IterativeClosestPoint_Gen_XYZN(boost::shared_ptr<std::vector<std::string>> argList);
	void RegionGrowingClustering_XYZ(boost::shared_ptr<std::vector<std::string>> argList);
	void Visualize_XYZ(boost::shared_ptr<std::vector<std::string>> argList);
	void Visualize_XYZN(boost::shared_ptr<std::vector<std::string>> argList);
#pragma endregion
}
