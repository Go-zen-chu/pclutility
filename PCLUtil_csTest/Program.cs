﻿using PCLUtil_cs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace PCLUtil_csTest
{
    class Program
    {
        public static string GetProjectDirPath()
        {
            var execPath = Environment.CurrentDirectory; // 実行ファイルのパス 
            var extractingDirName = "PCLUtil_csTest"; // プロジェクト名
            var extractingStr = Path.DirectorySeparatorChar + extractingDirName + Path.DirectorySeparatorChar;
            var idx = execPath.LastIndexOf(extractingStr);
            var projectPath = execPath.Substring(0, idx + extractingStr.Length);
            //Console.WriteLine(projectPath); // => "C:\Users\user\Desktop\SolutionName\YourProjectName\"
            return projectPath;
        }

        static void Main(string[] args)
        {
            var projPath = GetProjectDirPath();
            var dataPath = Path.Combine(projPath, "data");
            var bunnyPath = Path.Combine(dataPath, "bunny.pcd");
            var dskPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);


            //var depthImgPath = @"E:\IdentifyUserFromTop\DepthImages\201507151935403733\201507151936000940.png";
            //var depthImg = new WriteableBitmap(new BitmapImage(new Uri(depthImgPath)));
            //var pcdPath = Path.Combine(dskPath, "b.pcd");

            //PCLUtil_cs.PCLUtil_cpp.RegionGrowingClusteringXYZ(pcdPath, 30);

            //var filteredPath = Path.Combine(dataPath, "bunny_filtered.pcd");
            //PCLUtil_cpp.FilterPointCloud_XYZ(bunnyPath, filteredPath, "x", 0, 0.03);
            //PCLUtil_cpp.Visualize_XYZ(filteredPath);

            //var downsampledPath = Path.Combine(dataPath, "bunny_dwn.pcd");
            //PCLUtil_cpp.VoxelGridDownSample_XYZ(bunnyPath, downsampledPath, 0.03);
            //PCLUtil_cpp.Visualize_XYZ(downsampledPath);

            //var smoothedPath = Path.Combine(dataPath, "bunny_smth.pcd");
            //PCLUtil_cpp.SmoothPointCloud_XYZ_XYZN(bunnyPath, smoothedPath, 0.02);
            //PCLUtil_cpp.Visualize_XYZN(smoothedPath);

            var bunny_nPath = @"C:\Users\headl\Desktop\bunny_n.pcd";//Path.Combine(dataPath, "bunny_n.pcd"); // calc normal
            //var surfaceArea = PCLUtil_cpp.CalcSurfaceArea_XYZN(bunny_nPath, 0.1, 2.5);
            //Console.WriteLine("surface area: " + surfaceArea);

            Console.WriteLine("Ave Curv : " + PCLUtil_cs.PCLUtil_cpp.GetAverageCurvature_XYZN(bunny_nPath));

            Console.ReadKey();
        }
    }

    
}
